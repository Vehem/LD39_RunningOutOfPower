﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


using System;
public class AutoType : MonoBehaviour {

	public float letterPause = 0.001f;
	public float newLinePause = 1f;
	public AudioClip sound;
	AudioSource sfx;

	bool secondPartShown;

	Text typeText;
	IEnumerator typer;

	string message;

	// Use this for initialization
	void Start () {
		typeText = GetComponent<Text> ();
		sfx = GetComponent<AudioSource> ();
		message = typeText.text;
		typeText.text = "";
		StartCoroutine(TypeText ());

	}

	IEnumerator TypeText () {
		foreach (char letter in message.ToCharArray()) {
			typeText.text += letter;
			if (letter == '\n')
				yield return new WaitForSeconds (newLinePause);
			else if (sound)
				sfx.PlayOneShot (sound);
			yield return 0;
			yield return new WaitForSeconds (letterPause);
		}      
	}
}