﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		SharedObject.ClearAll ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Easy()
	{
		SharedObject.SetString ("Difficulty", "Easy");
		StartGame ();
	}

	public void Medium()
	{
		SharedObject.SetString ("Difficulty", "Normal");
		StartGame ();
	}

	public void Hard()
	{
		SharedObject.SetString ("Difficulty", "Hard");
		StartGame ();
	}

	void StartGame()
	{
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex + 1);	
	}

	public void Quit()
	{
		Application.Quit ();
	}

}
