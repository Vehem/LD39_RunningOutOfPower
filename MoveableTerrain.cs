﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MoveableTerrain : MonoBehaviour {

	float railgunforce = 10f;
	Rigidbody rb;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ImpactForce(Vector3 point, Vector3 direction)
	{
		rb.AddForceAtPosition (direction.normalized * railgunforce, point, ForceMode.Impulse);
	}
}
