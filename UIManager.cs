﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public static UIManager instance;

	public Text missionComplete;
	public Text powerFailure;

	public Image fadeToBlack;
	float fadeAlpha;
	float fadeDir = 1f;
	float fadeSpeed = 0.2f;
	bool fading = false;

	// Use this for initialization
	void Awake() {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {

		if (fading)
		{
			fadeAlpha += fadeDir * fadeSpeed * Time.deltaTime;  
			fadeAlpha = Mathf.Clamp01(fadeAlpha);
			fadeToBlack.color = new Color(fadeToBlack.color.r, fadeToBlack.color.b, fadeToBlack.color.g, fadeAlpha);

			if ((fadeDir == -1 && fadeAlpha == 0) || (fadeDir == 1 && fadeAlpha == 1))
			{
				fading = false;
			}
		}
	}

	public void DisplayMissionComplete(bool display)
	{
		missionComplete.enabled = display;
	}

	public void FadeToBlack(bool fadeOut)
	{
		
		fading = true;
		if (fadeOut)
			fadeDir = 1f;
		else
			fadeDir = -1f;
	}

	public void DisplayPowerFailure(bool display)
	{
		powerFailure.enabled = display;
		SoundManager.instance.Silence ();
		SoundManager.instance.PlaySFX (SoundManager.instance.powerDown);
	}
}
