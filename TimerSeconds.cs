﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class TimerSeconds : MonoBehaviour {

	Text time;

	// Use this for initialization
	void Start () {
		time = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {

		time.text = ((int)Time.timeSinceLevelLoad % 60).ToString ("D2");
	}
}
