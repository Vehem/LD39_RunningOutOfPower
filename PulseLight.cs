﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseLight : MonoBehaviour {

	Light light;

	// Use this for initialization
	void Start () {
		light = GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {

		light.intensity = Mathf.Lerp(0, 10, Mathf.PingPong (Time.time*5, 1f));

	}
}
