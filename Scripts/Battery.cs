﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Battery : MonoBehaviour {

	public int batteryValue;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter(Collision coll)
	{
		if (coll.collider.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (batteryValue);
			Destroy (gameObject);
		}
	}

}

