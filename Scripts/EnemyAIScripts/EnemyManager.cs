﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyManager : MonoBehaviour {

	public static EnemyManager instance;

	public List<EnemyBase> enemyList = new List<EnemyBase>();

	float lastSpawn = 0f;
	public float timeBetweenSpawns = 4f;

	public GameObject prefabEnemy;
	public GameObject prefabEnemyShoot;
	public GameObject prefabEnemyTough;
	public GameObject prefabEnemySkirmisher;

	public GameObject prefabSpawnWarning;
	public GameObject prefabSpawnEffect;
	public List<GameObject> spawnWarnings = new List<GameObject>();

	[Header("Level spawn locations")]
	public float minX;
	public float minZ;
	public float maxX; 
	public float maxZ;

	public List<EnemySpawn> spawns = new List<EnemySpawn>();
	bool allSpawned;
	public bool infiniteLevel = false;

	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {

		if (infiniteLevel)
		{
			if (lastSpawn + timeBetweenSpawns < Time.timeSinceLevelLoad)
			{
				PreSpawnEnemy (RandomLevelLocation (), null);
				lastSpawn = lastSpawn + timeBetweenSpawns;
				timeBetweenSpawns *= 0.975f;
				if (timeBetweenSpawns < 0.5f)
					timeBetweenSpawns = 0.5f;
			}
			return;
		}

		if (!allSpawned)
			CheckSpawn ();
		
		if (allSpawned && enemyList.Count == 0 && spawnWarnings.Count == 0)
		{
			UIManager.instance.DisplayMissionComplete (true);
			UIManager.instance.FadeToBlack (true);
			Invoke("CallNextLevel", 5f);
		}
	}

	void CallNextLevel()
	{
		GameRules.instance.NextLevel ();
	}

	void CheckSpawn()
	{
		allSpawned = true;
		foreach (EnemySpawn spawn in spawns)
		{
			if (spawn.time != -1)
			{
				allSpawned = false;
				if (Time.timeSinceLevelLoad > spawn.time)
				{	
					for (int i = 0; i < spawn.count; i++)
					{
						PreSpawnEnemy (RandomLevelLocation (), spawn.enemy);
						spawn.time = -1;
					}
				}
			}
		}
	}

	Vector3 RandomLevelLocation ()
	{
		return RandomPoint (Vector3.zero, 20f);

		//return (new Vector3 (Random.Range(-20f, 20f), 0.5f, Random.Range(-20f, 20f)));

	}

	Vector3 RandomPoint(Vector3 center, float range) {
		//	for (int i = 0; i < 1000; i++) {
		//		Debug.Log ("i = " + i);
		//		Vector3 randomPoint = center + Random.insideUnitSphere * range;
		//		Debug.DrawRay (randomPoint, Vector3.up, Color.blue, 1f);
		//		NavMeshHit hit;
		//		if (NavMesh.SamplePosition(randomPoint, out hit, 1.0f, 1 << NavMesh.GetAreaFromName("Walkable"))) 
		//		{
		//			Debug.DrawRay (hit.position, Vector3.up, Color.red, 1f);
		//			result = hit.position;
		//			return true;
		//		}
		//	}
		//	Debug.Log ("returning false");
		//	result = Vector3.zero;
		//	return false;

		Vector3 randomPoint = new Vector3 (Mathf.Infinity, Mathf.Infinity, Mathf.Infinity);

		for (int i = 0; i < 1000; i++)
		{
			randomPoint = new Vector3 (Random.Range (minX, maxX), 0.5f, Random.Range (minZ, maxZ));
			Debug.DrawRay (randomPoint, Vector3.up*3f, Color.blue, 1f);

			Vector3 above = randomPoint + new Vector3 (0f, 30f, 0f);
			Ray ray = new Ray (above, randomPoint - above);
			RaycastHit hit;

			if (Physics.Raycast (ray, out hit, 100f))
			{
				if (hit.collider.tag == "Floor")
				{
					break;
				}
			}
		}

		return randomPoint;

	}


	void PreSpawnEnemy(Vector3 location, GameObject enemyToSpawn)
	{
		location.y = 0.5f;
		GameObject spawnWarning = GameObject.Instantiate (prefabSpawnWarning, transform);
		spawnWarning.transform.position = location;
		spawnWarnings.Add (spawnWarning);
		StartCoroutine(SpawnEnemy (spawnWarning, enemyToSpawn));
	}

	IEnumerator SpawnEnemy(GameObject spawnWarning, GameObject enemyToSpawn)
	{
		yield return new WaitForSeconds (2f);

		GameObject spawnEffect = GameObject.Instantiate (prefabSpawnEffect, transform);
		spawnEffect.transform.position = spawnWarning.transform.position;
		GameObject newEnemy;

		if (enemyToSpawn == null)
		{
			int enemySpawnRoll = Random.Range (0, 100);
			if (enemySpawnRoll < 50)
				newEnemy = GameObject.Instantiate (prefabEnemy, transform);
			else if (enemySpawnRoll < 80)
				newEnemy = GameObject.Instantiate (prefabEnemyShoot, transform);
			else if (enemySpawnRoll < 90)
				newEnemy = GameObject.Instantiate (prefabEnemyTough, transform);
			else
				newEnemy = GameObject.Instantiate (prefabEnemySkirmisher, transform);
		} 
		else
			newEnemy = GameObject.Instantiate (enemyToSpawn, transform);
	
		newEnemy.transform.position = spawnEffect.transform.position;
		enemyList.Add (newEnemy.GetComponent<EnemyBase>());

		spawnWarnings.Remove (spawnWarning);
		Destroy (spawnWarning);
		yield return new WaitForSeconds (0.5f);
		Destroy (spawnEffect);
	}

//	public void RemoveEnemy(Enemy enemy)
//	{
//		enemyList.Remove (enemy);
//	}

	public EnemyBase NearestEnemyTo(Vector3 pos)
	{
		float nearestDistance = Mathf.Infinity;
		EnemyBase nearestEnemy = null;

		foreach (EnemyBase enemy in enemyList)
		{
			float sqrDist = Vector3.SqrMagnitude (pos - enemy.transform.position);
			if (sqrDist < nearestDistance)
			{
				nearestDistance = sqrDist;
				nearestEnemy = enemy;
			}
		}

		return nearestEnemy;
	}
}

[System.Serializable]
public class EnemySpawn
{
	[SerializeField]
	public int count;
	[SerializeField]
	public GameObject enemy;
	[SerializeField]
	public int time;
}
