﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyTough : EnemyBase {

	NavMeshAgent navAgent;
	GameObject player;
	public GameObject batteryPrefab;

	public GameObject shield;

	public int hitPoints = 3;
	public int dropChance; 

	SoundManager sfx;
	Material shieldMaterial;

	float lastShieldDamage = 0f;
	float shieldRechargeTime = 4f;
	Color shieldFull = new Color(0f, 192f/255f, 1f, 0.3f);
	Color shieldDamaged = new Color (0.75f, 0f, 0f, 0.3f);

	// Use this for initialization
	void Start () {
		navAgent = GetComponent<NavMeshAgent> ();
		player = GameObject.Find ("Player");
		sfx = SoundManager.instance;
		shieldMaterial = shield.GetComponent<Renderer> ().material;

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
		{
			navAgent.speed *= 1.4f;
			shieldRechargeTime = 2.5f;
		}

	}

	// Update is called once per frame
	void FixedUpdate () {
		DoMovement ();
		if (shield != null)
			UpdateShield ();
	}

	protected void DoMovement()
	{
		if (navAgent.enabled == false)
			navAgent.enabled = true;
		navAgent.SetDestination (player.transform.position);
	}

	public override void TakeHit()
	{
		hitPoints--;

		lastShieldDamage = Time.time;

		if (hitPoints > 0)
			return;

		sfx.PlaySFX (sfx.enemyDie);
		EnemyManager.instance.enemyList.Remove (this);

		GameRules.instance.CheckForItemDrop (dropChance, transform.position);

		Destroy (gameObject);

	}

	void UpdateShield()
	{
		if (GameRules.instance.difficultySetting != GameRules.Difficulty.Easy)
		{
			if (hitPoints < 3 && Time.time > lastShieldDamage + shieldRechargeTime)
			{
				hitPoints++;
				lastShieldDamage = Time.time;
			}
		}

		if (hitPoints == 3)
		{
			shieldMaterial.color = shieldFull;
		}
		if (hitPoints == 2)
		{
			shieldMaterial.color = shieldDamaged;
		}
		if (hitPoints == 1)
		{
			Destroy(shield);
		}

	}

	void OnCollisionStay(Collision coll)
	{
		if (coll.collider.gameObject.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (-35f * Time.deltaTime);
		}
	}
}
