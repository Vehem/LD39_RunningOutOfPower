﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : EnemyBase {

	NavMeshAgent navAgent;
	GameObject player;
	public GameObject batteryPrefab;

	public int dropChance;

	SoundManager sfx;

	// Use this for initialization
	void Start () {
		navAgent = GetComponent<NavMeshAgent> ();
		player = GameObject.Find ("Player");
		sfx = SoundManager.instance;

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
		{
			navAgent.speed *= 1.4f;
		}
	}

	// Update is called once per frame
	void FixedUpdate () {
		DoMovement ();
	}

	protected void DoMovement()
	{
		if (navAgent.enabled == false)
			navAgent.enabled = true;

		navAgent.SetDestination (player.transform.position);
	}

	public override void TakeHit()
	{
		sfx.PlaySFX (sfx.enemyDie);
		EnemyManager.instance.enemyList.Remove (this);

		GameRules.instance.CheckForItemDrop (dropChance, transform.position);

		Destroy (gameObject);

	}

	void OnCollisionStay(Collision coll)
	{
		if (coll.collider.gameObject.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (-25f * Time.deltaTime);
		}
	}
}
