﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyShoot : EnemyBase {

	GameObject player;
	public GameObject batteryPrefab;
	public GameObject bullet;   
	public Transform firepoint;

	public int dropChance;

	SoundManager sfx;

	float nextShotAttempt = 1f;
	public float delayBetweenShotAttempts = 0.5f;
	public float delayAfterTakingShot = 5f;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
		sfx = SoundManager.instance;

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
		{
			delayAfterTakingShot = 3f;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (nextShotAttempt < Time.time)
		{
			AttemptShot ();
		}
	}

	void DoMovement()
	{
		
	}

	void AttemptShot()
	{
		RaycastHit hit;
		Ray ray = new Ray (firepoint.position, player.transform.position - transform.position);

		Physics.Raycast (ray, out hit, 100f);

		if (hit.collider)
		{
			if (hit.collider.gameObject.tag == "Player")
			{
				GameObject bullet_go = Instantiate (bullet, firepoint.position, Quaternion.identity);
				bullet_go.GetComponent<Bullet> ().destination = player.transform.position;
				nextShotAttempt = Time.time + delayAfterTakingShot;
				sfx.PlaySFX (sfx.enemyLaser);
			} else
				nextShotAttempt = Time.time + delayBetweenShotAttempts;

		}

	}

	public override void TakeHit()
	{
		sfx.PlaySFX (sfx.enemyDie);
		EnemyManager.instance.enemyList.Remove (this);

		GameRules.instance.CheckForItemDrop (dropChance, transform.position);

		Destroy (gameObject);

	}

	void OnCollisionStay(Collision coll)
	{
		if (coll.collider.gameObject.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (-25f * Time.deltaTime);
		}
	}
}