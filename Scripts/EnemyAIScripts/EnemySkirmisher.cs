﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySkirmisher : EnemyBase {

	NavMeshAgent navAgent;
	GameObject player;
	public GameObject batteryPrefab;
	public GameObject bullet;

	public int dropChance;

	SoundManager sfx;

	// Use this for initialization
	void Start () {
		navAgent = GetComponent<NavMeshAgent> ();
		player = GameObject.Find ("Player");
		sfx = SoundManager.instance;
		navAgent.updateRotation = false;

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
		{
			navAgent.speed *= 5f;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		DoMovement ();
	}


	void DoMovement()
	{
		if (navAgent.enabled == false)
		{
			navAgent.enabled = true;
			navAgent.SetDestination(new Vector3(Random.Range(-20f, 20f), 0f, Random.Range(-20f, 20f)));
		}
		
		Vector3 heading = player.transform.position - transform.position;
		Vector2 flatHeading = new Vector2 (heading.x, heading.z).normalized;
		float angle = Mathf.Atan2 (flatHeading.x, flatHeading.y) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis (angle, Vector3.up), Time.deltaTime * 10f);

		if (Vector3.Distance (transform.position, navAgent.destination) < 4f)
		{
			AttemptShot ();
			navAgent.SetDestination(new Vector3(Random.Range(-20f, 20f), 0f, Random.Range(-20f, 20f)));

		}
	}

	void AttemptShot()
	{
		RaycastHit hit;
		Ray ray = new Ray (transform.position, player.transform.position - transform.position);

		Physics.Raycast (ray, out hit, 100f);

		if (hit.collider)
		{
			if (hit.collider.gameObject.tag == "Player")
			{
				GameObject bullet_go = Instantiate (bullet, transform.position, Quaternion.identity);
				bullet_go.GetComponent<Bullet> ().destination = player.transform.position;
				sfx.PlaySFX (sfx.enemyLaser);
			} 

		}

	}

	public override void TakeHit()
	{
		sfx.PlaySFX (sfx.enemyDie);
		EnemyManager.instance.enemyList.Remove (this);

		GameRules.instance.CheckForItemDrop (dropChance, transform.position);

		Destroy (gameObject);

	}

	void OnCollisionStay(Collision coll)
	{
		if (coll.collider.gameObject.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (-25f * Time.deltaTime);
		}
	}
}