﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergySync : MonoBehaviour {

	Slider energyBar;
	Image bluebar;
	Color barColor; 



	public Image criticalText;

	// Use this for initialization
	void Start () {
		energyBar = GetComponent<Slider> ();
		bluebar = energyBar.fillRect.GetComponent<Image> ();
		barColor = new Color (0, 200f/255f, 1f);
	}

	// Update is called once per frame
	void Update () {

		energyBar.value = EnergyManager.instance.GetCurrentEnergyFraction ();

		if (energyBar.value <= 0.2f)
		{
			bluebar.color = Color.Lerp (Color.red, Color.black, Mathf.PingPong (Time.time, 0.5f));
			criticalText.color = Color.Lerp (Color.white, Color.yellow, Mathf.PingPong (Time.time*5f, 1f));
		}
		else
		{
			bluebar.color = barColor;
			criticalText.color = Color.white;
		}
	}
}
