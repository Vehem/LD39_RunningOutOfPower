﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneBob : MonoBehaviour {

	public float bobAmount;
	float halfBobAmount;
	public float bobFrequency;
	float originalHeight;



	// Use this for initialization
	void Start () {
		halfBobAmount = bobAmount /2f;
		originalHeight = transform.localPosition.y;
	}
	
	// Update is called once per frame
	void Update () {
		transform.localPosition = new Vector3 (transform.localPosition.x, originalHeight + Mathf.PingPong (Time.time*bobFrequency, bobAmount) - halfBobAmount, transform.localPosition.z);
	}
}
