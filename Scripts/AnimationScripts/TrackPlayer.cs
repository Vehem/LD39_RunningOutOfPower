﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayer : MonoBehaviour {

	GameObject player;
	public float trackSpeed;

	// Use this for initialization
	void Start () {
		player = GameObject.Find ("Player");
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 heading = player.transform.position - transform.position;
		Vector2 flatHeading = new Vector2 (heading.x, heading.z).normalized;
		float angle = Mathf.Atan2 (flatHeading.x, flatHeading.y) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis (angle, Vector3.up), Time.deltaTime * trackSpeed);
	}
}
