﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTracking : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		LookAtNearestEnemy ();
	}

	void LookAtNearestEnemy()
	{

		EnemyBase nearestEnemy = EnemyManager.instance.NearestEnemyTo (transform.position);

		if (nearestEnemy != null)
		{
			Vector3 heading = nearestEnemy.transform.position - transform.position;
			Vector2 flatHeading = new Vector2 (heading.x, heading.z).normalized;
			float angle = Mathf.Atan2 (flatHeading.x, flatHeading.y) * Mathf.Rad2Deg;
			transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.AngleAxis (angle, Vector3.up), Time.deltaTime * 10f);
		}
	}
}
