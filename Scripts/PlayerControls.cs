﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerControls : MonoBehaviour {

	public float turnSpeed = 90f;
	public float moveSpeed = 10f;
	public float basePowerDrain = 1f;

	Rigidbody rb;
	LineRenderer lr;
	bool firing = false;

	float powerDrainThisFrame;

	SoundManager sfx;

	public GameObject torso;
	public GameObject tracks;

	public Transform firepoint;
	public GameObject beamHitPrefab;

	Vector3 lastMovementVector;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();

		lr = GetComponent<LineRenderer> ();
		lr.startWidth = 0.2f;
		lr.endWidth = 0.2f;

		sfx = SoundManager.instance;

	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!GameRules.instance.playerHasPower)
			return;

		powerDrainThisFrame = basePowerDrain;

		CheckMovement ();
		LookAtMouse ();
		CheckShooting ();

		DrainPower ();
	}


	void LookAtMouse () 
	{
		Plane playerPlane = new Plane(Vector3.up, transform.position);
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		float hitdist = 0.0f;
		if (playerPlane.Raycast (ray, out hitdist)) 
		{
			Vector3 targetPoint = ray.GetPoint(hitdist);

			if (Vector3.Distance(targetPoint, transform.position) < 2.5f)
				return;
	
			//Vector3 heading = targetPoint - transform.position;
			Vector3 heading = targetPoint - firepoint.position;
			Vector2 flatHeading = new Vector2 (heading.x, heading.z).normalized;
			float angle = Mathf.Atan2(flatHeading.x, flatHeading.y) * Mathf.Rad2Deg;
			//transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
			torso.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
		}
	}

	void CheckMovement()
	{
		Vector3 movement = new Vector3 (0f, 0f, 0f);
		if (Input.GetKey (KeyCode.W))
		{
			movement.x += 1f;
			movement.z += -1f;
		}
		if (Input.GetKey (KeyCode.A))
		{
			movement.x += 1f;
			movement.z += 1f;
		}
		if (Input.GetKey (KeyCode.S))
		{
			movement.x += -1f;
			movement.z += 1f;
		}
		if (Input.GetKey (KeyCode.D))
		{
			movement.x += -1f;
			movement.z += -1f;
		}
		movement *= Time.deltaTime * moveSpeed;

		if (movement.x != 0 || movement.z != 0)
		{
			powerDrainThisFrame += 4;
		}

		rb.MovePosition(transform.position + movement);



		if (movement == Vector3.zero)
		{
			movement = lastMovementVector;
		}

		Vector2 flatHeading = new Vector2 (movement.x, movement.z).normalized;
		float angle = Mathf.Atan2 (flatHeading.x, flatHeading.y) * Mathf.Rad2Deg;
		tracks.transform.rotation = Quaternion.AngleAxis (angle, Vector3.up);

		lastMovementVector = movement;

	}

	void DrainPower()
	{
		EnergyManager.instance.ModifyEnergy (-powerDrainThisFrame * Time.deltaTime);
	}

	void CheckShooting()
	{
		if (firing)
		{
			lr.SetPosition (0, firepoint.position);
			return;
		}

		if (Input.GetMouseButtonDown (0))
		{
			Vector3? impactPoint = null;
			/*
			RaycastHit beamhit;
			Ray ray = new Ray (transform.position, transform.forward);

			if (Physics.Raycast (ray, out beamhit, 100f, LayerMask.NameToLayer("Wall")))
			{
				if (beamhit.collider)
				{
					impactPoint = beamhit.point;
					Debug.Log ("Impact at " + impactPoint.ToString());
				}
			}*/

			List<EnemyBase> enemiesHit = new List<EnemyBase> ();

			RaycastHit[] hits;
			hits = Physics.RaycastAll(firepoint.position, firepoint.forward, 100.0F);
			List<RaycastHit>hitlist = new List<RaycastHit>(hits).OrderBy(h=>h.distance).ToList();
			hits=hitlist.ToArray();

			for (int i = 0; i < hits.Length; i++)
			{
				RaycastHit hit = hits[i];
				if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Enemy"))
				{
					enemiesHit.Add (hit.collider.gameObject.GetComponent<EnemyBase>());
				}
				if (hit.collider.gameObject.layer == LayerMask.NameToLayer("Wall"))
				{
					impactPoint = hit.point;
					MoveableTerrain moveable = hit.collider.gameObject.GetComponent<MoveableTerrain> ();
					if (moveable != null)
						moveable.ImpactForce (hit.point, hit.point - firepoint.position);

					break;
				}
			}
			foreach (EnemyBase enemy in enemiesHit)
			{
				enemy.TakeHit ();
			}

			//Debug.DrawLine (transform.position, transform.forward * 100f, Color.green, 0.2f);

			Debug.DrawLine (firepoint.position, (Vector3)impactPoint, Color.yellow, 0.3f);
			if (impactPoint != null)
				DrawLaserBeam ((Vector3)impactPoint);
			EnergyManager.instance.ModifyEnergy (-20f);
			sfx.PlaySFX (sfx.laser);

		}
	}

	void DrawLaserBeam(Vector3 targetPoint)
	{
		lr.SetPositions (new Vector3[] { firepoint.position, targetPoint});
		firing = true;
		Invoke ("ResetLaserBeam", 0.1f);
		GameObject beamHit = GameObject.Instantiate (beamHitPrefab, targetPoint-(targetPoint - firepoint.position).normalized* 1f, Quaternion.identity);
		Destroy (beamHit, 1f);
	}

	void ResetLaserBeam()
	{
		firing = false;
		lr.SetPositions (new Vector3[] {firepoint.position,firepoint.position});
	}
}
