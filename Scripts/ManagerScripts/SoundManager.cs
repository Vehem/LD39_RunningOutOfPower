﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
	public static SoundManager instance;
	// Use this for initialization

	public AudioClip laser;
	public AudioClip enemyLaser;
	public AudioClip enemyDie;
	public AudioClip powerDown;

	AudioSource sfx;

	void Awake () {
		instance = this;
		sfx = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Silence()
	{
		sfx.clip = null;
	}

	public void PlaySFX(AudioClip clip)
	{
		sfx.PlayOneShot (clip);	
	}
}
