﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class GameRules : MonoBehaviour {

	public bool playerHasPower = true;

	public enum Difficulty {Easy, Normal, Hard};

	public GameObject batteryPrefab;
	public GameObject largeBatteryPrefab;

	public Difficulty difficultySetting = Difficulty.Normal;

	public static GameRules instance;

	int lootMercyFactor = 0;

	// Use this for initialization
	void Start () {
		instance = this;

		if (SharedObject.GetString ("Difficulty") == "Easy")
			difficultySetting = Difficulty.Easy;

		if (SharedObject.GetString ("Difficulty") == "Hard")
			difficultySetting = Difficulty.Hard;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		
	public void CheckForItemDrop(int dropchance, Vector3 location)
	{
		dropchance += lootMercyFactor;

		if (difficultySetting == Difficulty.Easy)
			dropchance = (int)(dropchance * 1.5f);

		if (difficultySetting == Difficulty.Hard)
			dropchance = (int)(dropchance * 0.75f);

		int lootRoll = Random.Range (0, 100); 

		if (lootRoll < dropchance - 30)
		{
			GameObject.Instantiate (largeBatteryPrefab, location, Quaternion.identity);
			lootMercyFactor = 0;
		} else if (lootRoll < dropchance)
		{
			GameObject.Instantiate (batteryPrefab, location, Quaternion.identity);
			lootMercyFactor = 0;
		} 
		else
		{
			// Have mercy on the guy who didn't get a drop
			lootMercyFactor += 2;
		}
	}

	public void NextLevel()
	{
		SharedObject.SetFloat ("PlayerEnergy", EnergyManager.instance.GetCurrentEnergyValue ());

		CameraControl camControl = Camera.main.GetComponent<CameraControl> ();
		SharedObject.SetFloat ("CameraOffsetX", camControl.offset.x);
		SharedObject.SetFloat ("CameraOffsetY", camControl.offset.y);
		SharedObject.SetFloat ("CameraOffsetZ", camControl.offset.z);
		SharedObject.SetFloat ("CameraFOV", Camera.main.fieldOfView);



		int activeSceneIndex = SceneManager.GetActiveScene ().buildIndex;
		if (activeSceneIndex < SceneManager.sceneCountInBuildSettings-1)
		{
			SceneManager.LoadScene (activeSceneIndex + 1);
		}

	}

}
