﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class EnergyManager : MonoBehaviour {

	public static EnergyManager instance;
	public float startingEnergy;
	float currentEnergy;

	float pressureFactor = 1f;

	// Use this for initialization
	void Start () {
		instance = this;

		currentEnergy = SharedObject.GetFloat ("PlayerEnergy");
		if (currentEnergy > startingEnergy)
			currentEnergy = startingEnergy;


	}
	
	// Update is called once per frame
	void Update () {

		if (EnemyManager.instance.infiniteLevel)
		{
			pressureFactor = 1f + Time.timeSinceLevelLoad * 0.002f;
			Debug.Log ("Pressure Factor: " + pressureFactor);
		}

		if (currentEnergy <= 0)
		{
			GameRules.instance.playerHasPower = false;
			UIManager.instance.DisplayPowerFailure (true);
			UIManager.instance.FadeToBlack (true);
			Invoke ("GoMainMenu", 5f);
		}
	}

	void GoMainMenu()
	{
		SceneManager.LoadScene (0);
	}

	public float GetCurrentEnergyFraction()
	{
		return currentEnergy/startingEnergy;
	}

	public float GetCurrentEnergyValue()
	{
		return currentEnergy;
	}

	public void ModifyEnergy(float amount)
	{

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Easy)
			amount *= 0.75f;

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
			amount *= 1.5f;

		if (EnemyManager.instance.infiniteLevel)
			amount *= pressureFactor;

		currentEnergy += amount;

		if (currentEnergy > startingEnergy)
			currentEnergy = startingEnergy;

		if (currentEnergy < 0f)
			currentEnergy = 0f;
	}

}
