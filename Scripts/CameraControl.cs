﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Vector3 offset = new Vector3(-35f,40f,35f);
	public Transform player;
	public float scrollSpeed = 1f;

	Camera mainCamera;
	// Use this for initialization
	void Start () {
		mainCamera = Camera.main;

		if (SharedObject.GetFloat ("CameraOffsetX") != Mathf.Infinity)
		{
			offset.x = SharedObject.GetFloat ("CameraOffsetX");
			offset.y = SharedObject.GetFloat ("CameraOffsetY");
			offset.z = SharedObject.GetFloat ("CameraOffsetZ");
			mainCamera.fieldOfView = SharedObject.GetFloat ("CameraFOV"); 
		}
	}
	
	// Update is called once per frame
	void LateUpdate () {

		mainCamera.transform.position = player.transform.position + offset;
		mainCamera.transform.LookAt (player.transform);

		if (Input.mouseScrollDelta.y != 0)
		{
			mainCamera.fieldOfView += -Input.mouseScrollDelta.y * scrollSpeed;
			mainCamera.fieldOfView = Mathf.Clamp (Camera.main.fieldOfView, 10f, 50f);
		}

		if (Input.GetMouseButton (1))
		{
			float mouseMovement = Input.GetAxis ("Mouse Y");

			if ((mouseMovement < 0 && offset.z < 60) || (mouseMovement > 0 && offset.z > 10))
			{
				offset = offset + (mainCamera.transform.up * 2 + mainCamera.transform.forward) * mouseMovement * 4;
				mainCamera.fieldOfView += mouseMovement * scrollSpeed * 2;
			}
		}
	}
}
