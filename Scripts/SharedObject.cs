﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SharedObject {
	private static List<StringItem> stringCollections = new List<StringItem>();

	public static void SetString(string key, string value){
		foreach(StringItem stringItem in stringCollections){
			if(stringItem.key == key){
				stringCollections.Remove(stringItem);
				break;
			}
		}

		stringCollections.Add(new StringItem(key, value));
	}    

	public static string GetString(string key){
		string value = null;

		foreach(StringItem stringItem in stringCollections){
			if(stringItem.key == key){
				value = stringItem.value;
				break;
			}
		}

		return value;
	}

	private static List<FloatItem> floatCollections = new List<FloatItem>();

	public static void SetFloat(string key, float value){
		foreach(FloatItem floatItem in floatCollections){
			if(floatItem.key == key){
				floatCollections.Remove(floatItem);
				break;
			}
		}

		floatCollections.Add(new FloatItem(key, value));
	}    

	public static float GetFloat(string key){
		float value = Mathf.Infinity;

		foreach(FloatItem floatItem in floatCollections){
			if(floatItem.key == key){
				value = floatItem.value;
				break;
			}
		}

		return value;
	}

	public static void ClearAll()
	{
		stringCollections.Clear ();
		floatCollections.Clear ();
		intCollections.Clear ();
	}

	private static List<IntItem> intCollections = new List<IntItem>();

	public static void SetInt(string key, int value){
		foreach(IntItem intItem in intCollections){
			if(intItem.key == key){
				intCollections.Remove(intItem);
				break;
			}
		}

		intCollections.Add(new IntItem(key, value));
	}    

	public static int GetInt(string key){
		int value = int.MaxValue;

		foreach(IntItem intItem in intCollections){
			if(intItem.key == key){
				value = intItem.value;
				break;
			}
		}

		return value;
	}



	private class ObjectItem{
		private string _key;

		public ObjectItem(string key){
			this._key = key;
		}

		public string key{
			get{ return _key; }
		}
	}

	private class StringItem : ObjectItem{
		private string _value;

		public StringItem(string key, string value) : base(key){
			this._value = value;
		}

		public string value{
			get{ return _value; }
		}
	} 

	private class FloatItem : ObjectItem{
		private float _value;

		public FloatItem(string key, float value) : base(key){
			this._value = value;
		}

		public float value{
			get{ return _value; }
		}
	}

	private class IntItem : ObjectItem{
		private int _value;

		public IntItem(string key, int value) : base(key){
			this._value = value;
		}

		public int value{
			get{ return _value; }
		}
	}
}