﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	public Vector3 destination;
	public float speed;

	Vector3 direction;

	// Use this for initialization
	void Start () {
		direction = destination - transform.position;
		direction.y = 0;
		Destroy (gameObject, 5f);

		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Easy)
			speed = 10f;
		if (GameRules.instance.difficultySetting == GameRules.Difficulty.Hard)
			speed = 20f;
		
	}

	// Update is called once per frame
	void Update () {
		transform.position += direction.normalized * speed * Time.deltaTime;
	}

	void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.tag == "Wall")
			Destroy (gameObject);

		if (coll.gameObject.tag == "Player")
		{
			EnergyManager.instance.ModifyEnergy (-35f);
			Destroy (gameObject);
		}
	}
}
