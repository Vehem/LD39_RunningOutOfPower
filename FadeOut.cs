﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeOut : MonoBehaviour {

	Text text;
	bool fading = false;
	float fadeSpeed = -2f;
	float fadeAlpha = 1f;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
		Invoke ("Fade", 5f);
	}
	
	// Update is called once per frame
	void Update () {

		if (fading)
		{
			fadeAlpha += fadeSpeed * Time.deltaTime;  
			fadeAlpha = Mathf.Clamp01(fadeAlpha);
			text.color = new Color(text.color.r, text.color.b, text.color.g, fadeAlpha);

			if (fadeAlpha <= 0)
				Destroy (gameObject);
		}

	}

	void Fade()
	{
		fading = true;
	}
}
